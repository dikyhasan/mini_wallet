package util

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	uuid "github.com/satori/go.uuid"
)

func SetCorsService() cors.Config {

	corsConfig := cors.DefaultConfig()

	corsConfig.AllowOrigins = []string{"*"}
	corsConfig.AllowCredentials = true
	corsConfig.AddAllowMethods("OPTIONS")

	return corsConfig
}

func TimeNow() string {

	loc, _ := time.LoadLocation(LoadLocationDefault())

	DateTime := time.Now().In(loc).Format(time.RFC3339)

	return DateTime
}

func TimeAddSecond() string {

	loc, _ := time.LoadLocation(LoadLocationDefault())

	DateTime := time.Now().Add(4 * time.Second).In(loc).Format(time.RFC3339)

	return DateTime

}

func GenerateUUID(withHyphens bool) string {

	generatorUUID := uuid.NewV4()

	if withHyphens {
		return generatorUUID.String()
	} else {
		return strings.Replace(generatorUUID.String(), "-", "", -1)
	}
}

func SplitAuthentication(str string) (stringData []string) {

	strArr := strings.Split(str, " ")

	return strArr

}

func AllowAmount(number int) (result bool) {

	if number <= 0 {
		return false
	} else {
		return true
	}
}

func GetTagStruct(nameStruct interface{}, nameTag string) (result string) {

	result = reflect.TypeOf(nameStruct).Field(0).Tag.Get(nameTag)

	return result
}

func ConvertTimeFromRFC3339(str string) (resultString string) {

	// format RFC3339
	layout := "2006-01-02T15:04:05Z07:00"

	// str = "2022-01-28T20:12:43+07:00"
	result, _ := time.Parse(layout, str)

	resultString = result.String()

	return resultString
}

func CompareDate(today string, secondDate string) (result bool) {

	// Format RFC3339
	layout := "2006-01-02T15:04:05Z07:00"

	todayParse, _ := time.Parse(layout, today)
	secondDateParse, _ := time.Parse(layout, secondDate)

	// today
	return todayParse.After(secondDateParse)

}

func RandomRangeNumber(min int, max int) int {

	rand.Seed(time.Now().UnixNano())
	min = 2
	max = 5

	return rand.Intn(max-min+1) + min
}

func GenerateMarkQuery(count int) string {

	var Mark strings.Builder

	for i := 1; i <= count; i++ {

		Mark.WriteString("$")
		Mark.WriteString(strconv.Itoa(i))
		Mark.WriteString(",")

	}

	result := Mark.String()

	// remove last character , in string
	result = strings.TrimSuffix(result, ",")

	return result

}

func GenerateTokenAuth(key string) string {

	result := sha256.New()
	result.Write([]byte(key))
	sha1_hash := hex.EncodeToString(result.Sum(nil))

	return sha1_hash
}

func BindJSON(c *gin.Context, obj interface{}) error {
	if err := binding.JSON.Bind(c.Request, obj); err != nil {
		c.Error(err).SetType(gin.ErrorTypeBind)
		return err
	}
	return nil
}

func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func GetFileSize(filepath string) (int64, error) {
	fi, err := os.Stat(filepath)
	if err != nil {
		return 0, err
	}
	// get the size
	return fi.Size(), nil
}

func DirExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func CheckTotalPage(totalData int, showData int) (totalPage int) {

	if totalData != 0 {

		if showData > totalData {

			totalPage = 1

		} else {

			totalPage = totalData % showData

			if totalPage == 0 {

				totalPage = totalData / showData

			} else {

				totalPage = (totalData / showData) + 1
			}
		}
	} else {
		totalPage = 1
	}

	return totalPage

}

func EncryptPassword(pwd string) string {

	result := md5.Sum([]byte(pwd))
	return hex.EncodeToString(result[:])
}

func EncryptPasswordSHA256(key string) string {

	result := sha256.New()
	result.Write([]byte(key))
	sha1_hash := hex.EncodeToString(result.Sum(nil))

	return sha1_hash
}

func GetIntConvert(c *gin.Context) int {

	idParam := c.Param("id")
	id, _ := strconv.Atoi(idParam)
	return id

}

func GetIntConvertGlobal(c *gin.Context, param string) int {

	idParam := c.Param(param)
	id, _ := strconv.Atoi(idParam)
	return id

}

func PaddLeft(s string, pad string, plength int) string {
	for i := len(s); i < plength; i++ {
		s = pad + s
	}
	return s
}

func GetSettingResponseStatus() string {

	dataString := "status"
	return dataString
}

func GetSettingResponseMessage() string {

	dataString := "message"
	return dataString
}

func GetSettingResponseData() string {

	dataString := "data"
	return dataString
}

func GetSettingResponseTotalPage() string {

	dataString := "totalPage"
	return dataString
}

func GetSettingResponseTotalData() string {

	dataString := "totalData"
	return dataString
}

func GetSettingResponseCurrentPage() string {

	dataString := "currentPage"
	return dataString
}

func LoadLocationDefault() string {

	loc := "Asia/Jakarta"

	return loc
}

func CalculateLimitPaging(page int, showData int) (result int) {

	if page == 1 || page == 0 {
		result = 0
	} else {

		result = (page * showData) - showData
	}

	return result

}

func GetNowTime() string {

	loc, _ := time.LoadLocation(LoadLocationDefault())

	nowTime := time.Now().In(loc)

	var JoinPrefix strings.Builder
	var ResultPrefix string

	Hour := strconv.Itoa(nowTime.Hour())
	Minute := strconv.Itoa(nowTime.Minute())
	Second := strconv.Itoa(nowTime.Second())

	JoinPrefix.WriteString(Hour)
	JoinPrefix.WriteString(":")
	JoinPrefix.WriteString(Minute)
	JoinPrefix.WriteString(":")
	JoinPrefix.WriteString(Second)

	ResultPrefix = JoinPrefix.String()

	return ResultPrefix

}

func GetNowDate() string {

	loc, _ := time.LoadLocation(LoadLocationDefault())

	nowTime := time.Now().In(loc)

	// var JoinPrefix strings.Builder
	var ResultPrefix string

	ResultPrefix = nowTime.Format("2006-01-02")

	return ResultPrefix

}
