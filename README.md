# Mini e-wallet Project

## How to Run Application / Service:

- Service e-wallet Project Go (GIN)
- Migration 3 table:
  - my_wallet
  - history_deposits
  - history_wallet
- PostgreSQL + PGadmin

### 1. Copy file .env with default value (Example):

```
# Env Service
# 2022-01-27 13:52:16
GIN_MODE_PRODUCTION = "TRUE"
CONNECTDB="postgres://root:P@ssw0rddb@mini_wallet_database_container:5432/mini_wallet_db?sslmode=disable"

# Pgadmin Default
PGADMIN_DEFAULT_EMAIL="root@admin.com"
PGADMIN_DEFAULT_PASSWORD="P@ssw0rdpgadmin"
```

### 2. Run Service PostgreSQL:

```
docker-compose up -d mini_wallet_database
```

### 3. Run Service Migrate Container:

```
docker-compose up -d mini_wallet_migrate_db
```

### 4. Run Service PgAdmin:

```
docker-compose up -d mini_wallet_pgadmin
```

### 5. Run Service API:

```
docker-compose up -d --build
```

Thank you.
