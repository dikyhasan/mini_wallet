package main

import (
	"mini_wallet/routes"
)

func main() {

	// Setup Router
	router := routes.SetupRouter()

	// running
	router.Run(":4444")

}
