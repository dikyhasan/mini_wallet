FROM golang:1.17.6-alpine AS builder

# 2022-01-27 13:13:29
LABEL maintainer="Diky Hasan Al As'Ary"

RUN apk update && apk add --no-cache git tzdata ca-certificates openssl

WORKDIR /appserv-wallet

COPY . .

RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o app .

FROM scratch

WORKDIR /

COPY --from=builder /appserv-wallet/app .
COPY --from=builder /appserv-wallet/.env .
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /appserv-wallet/migrations/ migrations/

