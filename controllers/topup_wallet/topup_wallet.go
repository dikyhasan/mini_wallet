package topup_wallet

import (
	"log"
	"mini_wallet/models/model_status_wallet"
	"mini_wallet/models/model_topup_wallet"
	"mini_wallet/models/types/topup_wallet"
	"mini_wallet/util"
	"net/http"

	"github.com/gin-gonic/gin"
)

/**
* function init wallet for get token API KEY
 */
func TopUpAccount(c *gin.Context) {

	var RequestBody topup_wallet.RequestBodyTopup

	fieldStatus := util.GetSettingResponseStatus()
	fieldData := util.GetSettingResponseData()

	// // Valid API Key
	APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))
	APIKey := APIKeyArr[1]

	// Declaration
	response := gin.H{
		fieldStatus: "fail",
		fieldData:   "",
	}

	// Binding form-data
	err := c.ShouldBind(&RequestBody)

	if err != nil {

		log.Println("Error: read param: " + err.Error())

		response[fieldData] = err.Error()

		c.JSON(http.StatusBadRequest, response)
		c.Abort()

	} else {

		if (RequestBody == topup_wallet.RequestBodyTopup{}) {

			response[fieldData] = "Missing data for required field."

			c.JSON(http.StatusBadRequest, response)
			c.Abort()

		} else {

			// check status must is enabled
			resultCheck, err := model_status_wallet.StatusWallet(APIKey)

			if err != nil || !resultCheck {

				response[fieldStatus] = "fail"
				response[fieldData] = gin.H{
					"error": err.Error(),
				}

				c.JSON(http.StatusNotFound, response)
				return
			}

			// save token
			var Response topup_wallet.ResponseJSONTopup

			// Call model function
			Response, err = model_topup_wallet.TopupProcess(RequestBody, APIKey)

			if err != nil {

				response[fieldStatus] = "fail"
				response[fieldData] = gin.H{
					"error": err.Error(),
				}

				c.JSON(http.StatusBadRequest, response)
				c.Abort()

			} else {

				response[fieldStatus] = "success"
				response[fieldData] = gin.H{
					"deposit": Response,
				}

				c.JSON(http.StatusCreated, response)
			}

		}
	}
}
