package wallet

import (
	ModelReadWallet "mini_wallet/models/model_read_wallet"
	ModelWallet "mini_wallet/models/model_wallet"
	"mini_wallet/models/types/wallet"
	"mini_wallet/util"
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
* Function active wallet / enabled
 */
func ReadMyWallet(c *gin.Context) {

	var initWalletAccount wallet.ReadMyWallet

	// Get Valid token
	APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))
	token := APIKeyArr[1]

	fieldStatus := util.GetSettingResponseStatus()
	fieldData := util.GetSettingResponseData()

	// Declaration
	response := gin.H{
		fieldStatus: "fail",
		fieldData:   "",
	}

	// Call model function
	initWalletAccount, err := ModelReadWallet.GetInformationWallet(token)

	if (err != nil || initWalletAccount == wallet.ReadMyWallet{}) {

		response[fieldStatus] = "fail"
		response[fieldData] = gin.H{
			"error": "Disabled",
		}

		c.JSON(http.StatusNotFound, response)
		c.Abort()

	} else {

		response[fieldStatus] = "success"
		response[fieldData] = initWalletAccount

		c.JSON(http.StatusOK, response)
	}
}

/*
* Function active wallet / enabled
 */
func ActiveWallet(c *gin.Context) {

	var initWalletAccount wallet.WalletStatusJSONActive

	// Get Valid token
	APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))
	APIKey := APIKeyArr[1]

	fieldStatus := util.GetSettingResponseStatus()
	fieldData := util.GetSettingResponseData()

	// Declaration
	response := gin.H{
		fieldStatus: "fail",
		fieldData:   "",
	}

	// Call model function
	initWalletAccount, err := ModelWallet.ActiveWallet(APIKey)

	if (err != nil || initWalletAccount == wallet.WalletStatusJSONActive{}) {

		response[fieldStatus] = "fail"
		response[fieldData] = gin.H{
			"error": err.Error(),
		}

		c.JSON(http.StatusBadRequest, response)
		c.Abort()

	} else {

		response[fieldStatus] = "success"
		response[fieldData] = initWalletAccount

		c.JSON(http.StatusOK, response)
	}
}

/*
* Function disabled wallet
 */
func DisabledWallet(c *gin.Context) {

	var initWalletAccount wallet.WalletStatusJSONNonActive
	var WalletStatus wallet.WalletStatusForm

	// Get Valid token
	APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))
	APIKey := APIKeyArr[1]

	fieldStatus := util.GetSettingResponseStatus()
	fieldData := util.GetSettingResponseData()

	// Declaration
	response := gin.H{
		fieldStatus: "fail",
		fieldData:   "",
	}

	// get post param
	err := c.ShouldBind(&WalletStatus)

	GetInfStruct := util.GetTagStruct(WalletStatus, "form")

	if err != nil || WalletStatus.IsDisabled != true {

		response[fieldStatus] = "fail"
		response[fieldData] = gin.H{
			"error": gin.H{
				GetInfStruct: "Missing data for required field.",
			},
		}

		c.JSON(http.StatusBadRequest, response)
		return
	}

	// Call model function
	initWalletAccount, err = ModelWallet.NonActiveWallet(APIKey)

	if (err != nil || initWalletAccount == wallet.WalletStatusJSONNonActive{}) {

		response[fieldStatus] = "fail"
		response[fieldData] = gin.H{
			"error": err.Error(),
		}

		c.JSON(http.StatusBadRequest, response)
		c.Abort()

	} else {

		response[fieldStatus] = "success"
		response[fieldData] = initWalletAccount

		c.JSON(http.StatusOK, response)
	}
}
