package withdrawal

import (
	"log"
	"mini_wallet/models/model_status_wallet"
	"mini_wallet/models/model_withdrawal"
	"mini_wallet/models/types/withdrawal"
	"mini_wallet/util"
	"net/http"

	"github.com/gin-gonic/gin"
)

/**
* function init wallet for get token API KEY
 */
func WithDrawalProcess(c *gin.Context) {

	var RequestBody withdrawal.RequestBodyWithDrawal

	fieldStatus := util.GetSettingResponseStatus()
	fieldData := util.GetSettingResponseData()

	// // Valid API Key
	APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))
	APIKey := APIKeyArr[1]

	// Declaration
	response := gin.H{
		fieldStatus: "fail",
		fieldData:   "",
	}

	// Binding form-data
	err := c.ShouldBind(&RequestBody)

	if err != nil {

		log.Println("Error: read param: " + err.Error())

		response[fieldData] = err.Error()

		c.JSON(http.StatusBadRequest, response)
		c.Abort()

	} else {

		if (RequestBody == withdrawal.RequestBodyWithDrawal{}) {

			response[fieldData] = "Missing data for required field."

			c.JSON(http.StatusBadRequest, response)
			c.Abort()

		} else {

			// check status must is enabled
			resultCheck, err := model_status_wallet.StatusWallet(APIKey)

			if err != nil || !resultCheck {

				response[fieldStatus] = "fail"
				response[fieldData] = gin.H{
					"error": err.Error(),
				}

				c.JSON(http.StatusNotFound, response)
				return
			}

			var ResponseBody withdrawal.ResponseJSONWithDrawal

			// Call model function
			ResponseBody, err, result := model_withdrawal.ModelWithdrawalProcess(RequestBody, APIKey)

			if err != nil || !result {

				response[fieldStatus] = "fail"
				response[fieldData] = gin.H{
					"error": err.Error(),
				}

				c.JSON(http.StatusBadRequest, response)
				c.Abort()

			} else {

				response[fieldStatus] = "success"
				response[fieldData] = gin.H{
					"withdrawal": ResponseBody,
				}

				c.JSON(http.StatusCreated, response)
			}
		}
	}
}
