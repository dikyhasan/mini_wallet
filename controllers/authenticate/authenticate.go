package authenticate

import (
	"mini_wallet/models/model_auth"
	"mini_wallet/models/types/init_wallet"
	"mini_wallet/util"
	"net/http"

	"github.com/gin-gonic/gin"
)

func VerifyAuth(c *gin.Context) (APIKey string) {

	var initWallet init_wallet.InitCredentialToken
	// key for API Key
	APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))
	APIKey = APIKeyArr[1]

	// Call model function
	result, err := model_auth.AuthenticationAPIKey(initWallet.Token)

	if err != nil || result == false {

		c.JSON(http.StatusUnauthorized, gin.H{
			"status": http.StatusUnauthorized,
			"data": gin.H{
				"error": "Unauthorized",
			},
		})
		c.Abort()

	}

}
