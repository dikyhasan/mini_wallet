package init_wallet

import (
	"log"
	"mini_wallet/models/model_init_wallet"
	"mini_wallet/models/types/init_wallet"
	"mini_wallet/util"
	"net/http"

	"github.com/gin-gonic/gin"
)

/**
* function init wallet for get token API KEY
 */
func InitWalletAccount(c *gin.Context) {

	var initCreateToken init_wallet.InitCreateToken

	fieldStatus := util.GetSettingResponseStatus()
	fieldData := util.GetSettingResponseData()

	// Declaration
	response := gin.H{
		fieldStatus: "fail",
		fieldData:   "",
	}

	// Binding form-data
	err := c.ShouldBind(&initCreateToken)

	if err != nil {

		log.Println("Error: read param: " + err.Error())

		response[fieldData] = err.Error()

		c.JSON(http.StatusBadRequest, response)
		return

	} else {

		if (initCreateToken == init_wallet.InitCreateToken{}) {

			response[fieldData] = "customer_xid: Missing data for required field."

			c.JSON(http.StatusBadRequest, response)
			return

		} else {

			// save token
			var tokenStruct init_wallet.InitCredentialToken

			// Call model function
			tokenStruct.Token, err = model_init_wallet.CheckExistingCustomer(initCreateToken.CustomerXid)

			if (err != nil || tokenStruct == init_wallet.InitCredentialToken{}) {

				response[fieldStatus] = "fail"

				c.JSON(http.StatusBadRequest, response)
				return
			}

			response[fieldStatus] = "success"
			response[fieldData] = tokenStruct

			c.JSON(http.StatusCreated, response)

		}
	}
}
