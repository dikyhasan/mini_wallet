-- Table: public.my_wallet
BEGIN;

DROP TABLE IF EXISTS public.my_wallet;

CREATE TABLE IF NOT EXISTS public.my_wallet
(
    id uuid NOT NULL,
    customer_xid uuid NOT NULL,
    is_disabled boolean NOT NULL,
    balance numeric(12,0) NOT NULL,
    created_at character varying(50) NOT NULL,
    token character varying(40) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT wallets_pkey PRIMARY KEY (id),
    CONSTRAINT customer_xid_unique UNIQUE (customer_xid),
    CONSTRAINT token_unique UNIQUE (token)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.my_wallet
    OWNER to root;

GRANT ALL ON TABLE public.my_wallet TO root;

COMMIT;