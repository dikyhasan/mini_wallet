-- Table: public.history_deposits

BEGIN;

DROP TABLE IF EXISTS public.history_deposits;

CREATE TABLE IF NOT EXISTS public.history_deposits
(
    id uuid NOT NULL,
    reference_id uuid NOT NULL,
    customer_xid uuid NOT NULL,
    status character varying(25) COLLATE pg_catalog."default" NOT NULL,
    action_at character varying(50) NOT NULL,
    category_action character varying COLLATE pg_catalog."default",
    amount numeric(12,0),
    status_balance character varying COLLATE pg_catalog."default",
    time_update character varying(50),
    CONSTRAINT history_deposits_pkey PRIMARY KEY (id),
    CONSTRAINT reference_id_unique UNIQUE (reference_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.history_deposits
    OWNER to root;

GRANT ALL ON TABLE public.history_deposits TO root;

COMMIT;