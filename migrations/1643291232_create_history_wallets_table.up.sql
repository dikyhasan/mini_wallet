-- Table: public.history_wallets
BEGIN;

DROP TABLE IF EXISTS public.history_wallets;

CREATE TABLE IF NOT EXISTS public.history_wallets
(
    id uuid NOT NULL,
    customer_xid uuid NOT NULL,
    is_disabled boolean NOT NULL,
    action_at character varying(50) NOT NULL,
    CONSTRAINT history_wallets_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.history_wallets
    OWNER to root;

GRANT ALL ON TABLE public.history_wallets TO root;

COMMIT;