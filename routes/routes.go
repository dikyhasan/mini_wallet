package routes

import (
	"log"
	ControllerInit "mini_wallet/controllers/init_wallet"
	ControllerTopupWallet "mini_wallet/controllers/topup_wallet"
	ControllerWallet "mini_wallet/controllers/wallet"
	ControllerWithDrawal "mini_wallet/controllers/withdrawal"
	"mini_wallet/models/model_auth"
	"mini_wallet/util"
	"net/http"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/joho/godotenv"

	"github.com/gin-gonic/gin"
)

// SetUp Router
func SetupRouter() *gin.Engine {

	if err := godotenv.Load(".env"); err != nil {
		log.Println("Error: " + err.Error())
	}

	// GIN Mode
	if os.Getenv("GIN_MODE_PRODUCTION") == "TRUE" {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.Default()

	// Apply CORS Configuration
	SetCors := util.SetCorsService()
	router.Use(cors.New(SetCors))

	// Apply Gzip response
	router.Use(gzip.Gzip(gzip.DefaultCompression))

	// route list
	api := router.Group("/api/v1")

	// initialize
	api.POST("/init", ControllerInit.InitWalletAccount)

	api.GET("/wallet", AuthorizedCheck(), ControllerWallet.ReadMyWallet)

	// Enable my wallet
	api.POST("/wallet", AuthorizedCheck(), ControllerWallet.ActiveWallet)

	// Disabled my wallet
	api.PATCH("/wallet", AuthorizedCheck(), ControllerWallet.DisabledWallet)

	// Deposit my wallet
	api.POST("/wallet/deposits", AuthorizedCheck(), ControllerTopupWallet.TopUpAccount)

	// WithDrawal
	api.POST("/wallet/withdrawals", AuthorizedCheck(), ControllerWithDrawal.WithDrawalProcess)

	// default config for 405
	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "Resource not found."})
	})

	return router
}

/*
* Check Validatedata
 */
func AuthorizedCheck() gin.HandlerFunc {
	return func(c *gin.Context) {

		// key for API Key
		APIKeyArr := util.SplitAuthentication(c.Request.Header.Get("Authorization"))

		if len(APIKeyArr) == 2 {

			APIKey := APIKeyArr[1]

			if APIKeyArr[0] != "Token" || APIKeyArr[1] == "" {

				c.JSON(http.StatusUnauthorized, gin.H{
					"status": http.StatusUnauthorized,
					"data": gin.H{
						"error": "Unauthorized",
					},
				})
				c.Abort()
			} else {
				// check corection to database
				result, err := model_auth.AuthenticationAPIKey(APIKey)

				if err != nil || result == false {

					c.JSON(http.StatusUnauthorized, gin.H{
						"status": http.StatusUnauthorized,
						"data": gin.H{
							"error": "Unauthorized",
						},
					})
					c.Abort()
				}
			}
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"data": gin.H{
					"error": "Unauthorized",
				},
			})
			c.Abort()
		}
	}
}
