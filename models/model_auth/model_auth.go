package model_auth

import (
	"errors"
	"mini_wallet/config"
	InitWalletStruct "mini_wallet/models/types/init_wallet"
)

func AuthenticationAPIKey(token string) (result bool, err error) {

	var InitWallet InitWalletStruct.InitCredentialToken

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		err = errors.New("[AuthenticationAPIKey]: Connection failure.")

		return false, err

	}

	sql := `SELECT token FROM ` + TableName + ` WHERE token = $1`

	_ = db.QueryRow(sql, token).Scan(
		&InitWallet.Token)

	if (InitWallet == InitWalletStruct.InitCredentialToken{}) {

		return false, err
	} else {

		return true, err
	}

}
