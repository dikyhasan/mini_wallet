package model_read_wallet

import (
	"context"
	"errors"
	"log"
	"mini_wallet/config"
	"mini_wallet/models/model_topup_wallet"
	"mini_wallet/models/types/topup_wallet"
	"mini_wallet/models/types/wallet"
	"mini_wallet/util"
	"strconv"
)

func GetInformationWallet(token string) (WalletInformationJSONData wallet.ReadMyWallet, err error) {

	var WalletInformation wallet.ReadMyWallet

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		errors.New("[GetInformationWallet]: Connection failure.")
	}

	// run data Balance
	GetCustomerXid, _ := model_topup_wallet.GetCustomerXid(token)

	err = UpdateTopUpToBalance(GetCustomerXid)

	log.Println(err)

	sql := `
	SELECT id,customer_xid,  
	(CASE WHEN is_disabled = true THEN 'disabled' ELSE 'enabled' END) as status,
	(SELECT action_at FROM history_wallets WHERE history_wallets.customer_xid = my_wallet.customer_xid ORDER BY history_wallets.action_at LIMIT 1) as enable_at,
	balance 
	FROM ` + TableName + ` WHERE token = $1 AND is_disabled = false `

	_ = db.QueryRow(sql, token).Scan(
		&WalletInformation.Id,
		&WalletInformation.Owned_by,
		&WalletInformation.Status,
		&WalletInformation.EnableAt,
		&WalletInformation.Balance,
	)

	if (WalletInformation == wallet.ReadMyWallet{}) {

		return WalletInformation, err

	} else {

		return WalletInformation, err
	}
}

/*
* Update all deposits to balannce account
 */
func UpdateTopUpToBalance(CustomerXid string) (err error) {

	db, err := config.ConnectDatabase()
	defer db.Close()

	// initial
	totalBalance := 0
	TableName := "history_deposits"
	StatusAllUpdate := true
	todayTime := util.TimeNow()

	if err != nil {

		return errors.New("Connection failure.")
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {

		return errors.New("Context failure.")
	}

	rows, err := db.Query(`SELECT id, amount, time_update 
			 FROM `+TableName+` 
			 WHERE customer_xid = $1 AND status_balance = 'delay'`, CustomerXid)

	if err != nil {

		return errors.New("Query failure")
	}

	for rows.Next() {

		var TopupTime = topup_wallet.BalanceUpdate{}

		rows.Scan(
			&TopupTime.Id,
			&TopupTime.Amount,
			&TopupTime.TimeUpdate,
		)

		// filter date is empty
		// check data for topup after 4 second deposits
		if TopupTime.Id != "" && util.CompareDate(todayTime, TopupTime.TimeUpdate) == true {

			totalBalance += TopupTime.Amount

			// update status in table history deposits
			runQuery, _ := tx.PrepareContext(ctx, `UPDATE `+TableName+` SET status_balance = 'done' WHERE id = $1 `)

			_, err := runQuery.ExecContext(ctx, TopupTime.Id)

			if err != nil {
				StatusAllUpdate = false
			}
		}
	}

	// do update to balance
	runQueryBalance, err := tx.PrepareContext(ctx, `UPDATE my_wallet SET balance = balance + `+strconv.Itoa(totalBalance)+` WHERE customer_xid = $1`)

	log.Println(err)

	_, err = runQueryBalance.ExecContext(ctx, CustomerXid)

	log.Println(err)

	if err != nil {

		// rollback data
		tx.Rollback()
		return err

	} else {

		if StatusAllUpdate == true {
			tx.Commit()

			return nil

		} else {

			tx.Rollback()

			return err

		}
	}
}
