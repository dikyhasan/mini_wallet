package model_init_wallet

import (
	"context"
	"errors"
	"log"
	"mini_wallet/config"
	InitType "mini_wallet/models/types/init_wallet"
	"mini_wallet/util"
)

/*
* for check user double init then update token
 */
func CheckExistingCustomer(CustomerXid string) (token string, err error) {

	var initCheckCust InitType.InitProcessCust

	// initialized
	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		return "", errors.New("[CheckExistingCustomer]: Connection failure.")
	}

	sql := `SELECT id FROM my_wallet WHERE customer_xid = $1`

	_ = db.QueryRow(sql, CustomerXid).Scan(
		&initCheckCust.Id)

	if (initCheckCust == InitType.InitProcessCust{}) {

		// cust not exist insert action
		token, err := CreateWalletAccount(TableName, CustomerXid)

		return token, err
	} else {

		// cust exist update action
		errors.New("Customer existing.")

		token, err = UpdateExistingCustomer(TableName, initCheckCust.Id, CustomerXid)

		return token, err
	}
}

/**
* Create Credential Token
 */
func CreateCredentialToken(CustomerXid string) (result string, customer string, err error) {

	if CustomerXid == "" {
		return "", "", errors.New("Customer is invalid.")
	}

	// create token without hypen
	result = util.GenerateUUID(false)

	return result, CustomerXid, err

}

/*
* Create data wallet account and save to database
 */
func CreateWalletAccount(TableName string, CustomerXid string) (token string, err error) {

	// set mark query 6 column
	MarkQuery := util.GenerateMarkQuery(6)
	GenUUID := util.GenerateUUID(true)
	TimeNow := util.TimeNow()

	// default for wallet condition
	IsDisabledWallet := true

	Token, CustomerXid, err := CreateCredentialToken(CustomerXid)

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		return "", errors.New("[CreateWalletAccount]: Connection failure.")
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		return "", errors.New("[CreateWalletAccount]: Context failure.")
	}

	sql := `INSERT INTO ` + TableName + ` (id, customer_xid, is_disabled, balance, created_at, token) VALUES (` + MarkQuery + `)`

	// run execute sql
	runQuery, err := tx.PrepareContext(ctx, sql)

	if err != nil {
		log.Println("Err: " + err.Error())
	}

	// run query saving
	_, err = runQuery.ExecContext(ctx, GenUUID, CustomerXid, IsDisabledWallet, 0, TimeNow, Token)

	if err == nil {
		tx.Commit()
	} else {
		tx.Rollback()
		return "", errors.New("Failed insert data.")
	}

	return Token, err

}

/*
* for update token user existing
 */
func UpdateExistingCustomer(TableName string, Id string, CustomerXid string) (token string, err error) {

	db, err := config.ConnectDatabase()
	defer db.Close()

	Token, _, err := CreateCredentialToken(CustomerXid)

	if err != nil {
		return "", errors.New("[UpdateExistingCustomer]: Connection failure.")
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		return "", errors.New("[UpdateExistingCustomer]: Context failure.")
	}

	sql := `UPDATE ` + TableName + ` SET token = $1 WHERE id = $2`

	runQuery, err := tx.PrepareContext(ctx, sql)

	_, err = runQuery.ExecContext(ctx, Token, Id)

	if err == nil {
		tx.Commit()
	} else {
		tx.Rollback()
	}

	return Token, err
}
