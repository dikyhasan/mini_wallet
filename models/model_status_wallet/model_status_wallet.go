package model_status_wallet

import (
	"errors"
	"mini_wallet/config"
	"mini_wallet/models/model_topup_wallet"
	"mini_wallet/models/types/withdrawal"
)

/*
* Validate balance real for user / withdrawal
 */
func StatusWallet(APIKey string) (result bool, err error) {

	var AmountReal withdrawal.AmountReal

	TableName := "my_wallet"
	result = true

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {

		err = errors.New("[CheckBalanceReal]: Connection failure.")
		return false, err

	}

	CustomerXid, err := model_topup_wallet.GetCustomerXid(APIKey)

	if err != nil {
		return false, err
	}

	sql := `SELECT is_disabled FROM ` + TableName + ` WHERE customer_xid = $1 AND is_disabled = false `

	_ = db.QueryRow(sql, CustomerXid).Scan(
		&AmountReal.IsDisabled,
	)

	if AmountReal.IsDisabled {

		errors.New("Disabled.")
		return false, err
	}

	return result, err
}
