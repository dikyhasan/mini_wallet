package model_topup_wallet

import (
	"errors"
	"mini_wallet/config"
	"mini_wallet/models/types/init_wallet"
	"mini_wallet/models/types/topup_wallet"
	"mini_wallet/util"
)

/*
* Create data wallet account and save to database
 */
func TopupProcess(RequestBody topup_wallet.RequestBodyTopup, APIKey string) (Response topup_wallet.ResponseJSONTopup, err error) {

	TableName := "history_deposits"
	StatusProcess := ""

	// set mark query 6 column
	MarkQuery := util.GenerateMarkQuery(9)
	GenUUID := util.GenerateUUID(true)
	TimeNow := util.TimeNow()
	TimeAddSecond := util.TimeAddSecond()

	db, err := config.ConnectDatabase()
	defer db.Close()

	CustomerXid, err := GetCustomerXid(APIKey)

	if err != nil {
		return Response, err
	}

	if err != nil {
		err = errors.New("[TopupProcess]: Context failure.")
		return Response, err
	}

	// check amount
	if !util.AllowAmount(RequestBody.Amount) {

		err = errors.New("Amount is invalid.")
		return Response, err
	}

	sql := `INSERT INTO ` + TableName + ` (id, 
										   reference_id, 
										   customer_xid, 
										   status, 
										   action_at, 
										   category_action,
										   amount,
										   status_balance,
										   time_update) 
										   VALUES (` + MarkQuery + `)`

	// run execute sql
	runQuery, err := db.Prepare(sql)

	if err != nil {
		StatusProcess = "fail"
	} else {
		StatusProcess = "success"
	}

	// run query saving
	_, err = runQuery.Exec(
		GenUUID,
		RequestBody.ReferenceId,
		CustomerXid,
		StatusProcess,
		TimeNow,
		"deposit",
		RequestBody.Amount,
		"delay",
		TimeAddSecond,
	)

	// save to struct
	Response.Amount = RequestBody.Amount
	Response.Reference_id = RequestBody.ReferenceId
	Response.DepositedBy = CustomerXid
	Response.DepositedAt = TimeNow
	Response.StatusRequest = StatusProcess
	Response.Id = GenUUID

	if err != nil {

		err = errors.New("Failed push to history data wallets.")

		return Response, err
	}

	return Response, err

}

func GetCustomerXid(APIKey string) (customerXid string, err error) {

	var IniWallet init_wallet.InitCreateToken

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {

		err = errors.New("[VerifiyStatusWallet]: Connection failure.")
		return "", err
	}

	sql := `SELECT customer_xid FROM ` + TableName + ` WHERE token = $1 AND is_disabled = false `

	_ = db.QueryRow(sql, APIKey).Scan(
		&IniWallet.CustomerXid)

	if (IniWallet == init_wallet.InitCreateToken{}) {

		err = errors.New("Wallet disabled.")

		return IniWallet.CustomerXid, err
	} else {

		return IniWallet.CustomerXid, err
	}

}
