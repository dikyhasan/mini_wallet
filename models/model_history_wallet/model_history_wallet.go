package model_history_wallet

import (
	"context"
	"errors"
	"log"
	"mini_wallet/config"
	"mini_wallet/util"
)

/*
* Create data wallet account and save to database
 */
func PushHistoryWallet(CustomerXid string, ActionWallet bool) (err error) {

	TableName := "history_wallets"

	// set mark query 6 column
	MarkQuery := util.GenerateMarkQuery(4)
	GenUUID := util.GenerateUUID(true)
	TimeNow := util.TimeNow()

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		return errors.New("[CreateWalletAccount]: Connection failure.")
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		return errors.New("[CreateWalletAccount]: Context failure.")
	}

	sql := `INSERT INTO ` + TableName + ` (id, customer_xid, is_disabled, action_at) VALUES (` + MarkQuery + `)`

	// run execute sql
	runQuery, err := tx.PrepareContext(ctx, sql)

	if err != nil {
		log.Println("Err: " + err.Error())
	}

	// run query saving
	_, err = runQuery.ExecContext(ctx, GenUUID, CustomerXid, ActionWallet, TimeNow)

	if err == nil {
		tx.Commit()
	} else {
		tx.Rollback()
		return errors.New("Failed push to history data wallets.")
	}

	return err

}
