package withdrawal

type ResponseJSONWithDrawal struct {
	Id            string `json:"id"`
	Withdrawn     string `json:"withdrawn_by"`
	StatusRequest string `json:"status"`
	WithdrawnAt   string `json:"withdrawn_at"`
	Amount        int    `json:"amount"`
	Reference_id  string `json:"reference_id"`
}

type RequestBodyWithDrawal struct {
	Amount      int    `form:"amount" binding:"required"`
	ReferenceId string `form:"reference_id" binding:"required"`
}

type AmountReal struct {
	Amount     int
	IsDisabled bool
}
