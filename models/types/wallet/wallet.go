package wallet

type WalletActiveResponse struct {
	Id       string `json:"id"`
	Owned_by string `json:"owned_by"`
	StatusDB bool   `json:",omitempty"`
	Status   string `json:"status"`
	EnableAt string `json:"enable_at"`
	Balance  int    `json:"balance"`
}

type WalletNonActiveResponse struct {
	Id         string `json:"id"`
	Owned_by   string `json:"owned_by"`
	StatusDB   bool   `json:",omitempty"`
	Status     string `json:"status"`
	DisabledAt string `json:"disabled_at"`
	Balance    int    `json:"balance"`
}

type WalletStatusJSONActive struct {
	Id       string `json:"id"`
	Owned_by string `json:"owned_by"`
	Status   string `json:"status"`
	EnableAt string `json:"enable_at"`
	Balance  int    `json:"balance"`
}

type WalletStatusJSONNonActive struct {
	Id         string `json:"id"`
	Owned_by   string `json:"owned_by"`
	Status     string `json:"status"`
	DisabledAt string `json:"disabled_at"`
	Balance    int    `json:"balance"`
}

type WalletStatus struct {
	IsDisabled bool
}

type WalletStatusForm struct {
	IsDisabled bool `form:"is_disabled" binding:"required"`
}

type ReadMyWallet struct {
	Id       string `json:"id"`
	Owned_by string `json:"owned_by"`
	Status   string `json:"status"`
	EnableAt string `json:"enable_at"`
	Balance  int    `json:"balance"`
}

type MessageErrorInf struct {
	Errors []ItemsError `json:"error"`
}

type ItemsError struct {
	FieldError string
}
