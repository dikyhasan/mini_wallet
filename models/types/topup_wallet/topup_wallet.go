package topup_wallet

type ResponseJSONTopup struct {
	Id            string `json:"id"`
	DepositedBy   string `json:"deposited_by"`
	StatusRequest string `json:"status"`
	DepositedAt   string `json:"deposited_at"`
	Amount        int    `json:"amount"`
	Reference_id  string `json:"reference_id"`
}

type RequestBodyTopup struct {
	Amount      int    `form:"amount" binding:"required"`
	ReferenceId string `form:"reference_id" binding:"required"`
}

type BalanceUpdate struct {
	Id         string
	Amount     int
	TimeUpdate string
}
