package init_wallet

type InitCredentialToken struct {
	Token string `json:"token"`
}

type InitProcessCust struct {
	Id string
}

type InitCreateToken struct {
	CustomerXid string `form:"customer_xid" json:"customer_xid" binding:"required"`
}
