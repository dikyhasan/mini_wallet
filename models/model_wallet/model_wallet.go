package model_wallet

import (
	"context"
	"errors"
	"log"
	"mini_wallet/config"
	ModelHistoryWallet "mini_wallet/models/model_history_wallet"
	"mini_wallet/models/types/wallet"
)

func VerifiyStatusWallet(token string) (is_disabled bool, err error) {

	var WalletStatus wallet.WalletStatus

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		errors.New("[VerifiyStatusWallet]: Connection failure.")
	}

	sql := `SELECT is_disabled FROM ` + TableName + ` WHERE token = $1`

	_ = db.QueryRow(sql, token).Scan(
		&WalletStatus.IsDisabled)

	return WalletStatus.IsDisabled, err

}

/*
* for update token user existing
 */
func ActiveWallet(Token string) (WalletInformationJSONData wallet.WalletStatusJSONActive, err error) {

	var WalletInformationJSON wallet.WalletStatusJSONActive

	db, err := config.ConnectDatabase()
	defer db.Close()

	TableName := "my_wallet"

	if err != nil {

		err = errors.New("[ActiveNonActiveWallet]: Connection failure.")

		return WalletInformationJSON, err
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {

		err = errors.New("[ActiveNonActiveWallet]: Context failure.")

		return WalletInformationJSON, err
	}

	// check status wallet real database
	IsDisabledUpdate, err := CheckConditionWallet("enabled", Token)

	log.Println(IsDisabledUpdate)

	if err != nil {

		return WalletInformationJSON, err
	}

	sql := `UPDATE ` + TableName + ` SET is_disabled = $1 WHERE token = $2`

	runQuery, err := tx.PrepareContext(ctx, sql)

	_, err = runQuery.ExecContext(ctx, IsDisabledUpdate, Token)

	if err == nil {

		// commit data
		tx.Commit()

		// Get Detail Information Wallet
		WalletInformationJSON, _ = GetDetailInfWalletActive(Token)

		return WalletInformationJSON, err

	} else {
		tx.Rollback()
		return WalletInformationJSON, err
	}
}

func NonActiveWallet(Token string) (WalletInformationJSONData wallet.WalletStatusJSONNonActive, err error) {

	var WalletInformationJSON wallet.WalletStatusJSONNonActive

	db, err := config.ConnectDatabase()
	defer db.Close()

	TableName := "my_wallet"

	if err != nil {

		err = errors.New("[ActiveNonActiveWallet]: Connection failure.")

		return WalletInformationJSON, err
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {

		err = errors.New("[ActiveNonActiveWallet]: Context failure.")

		return WalletInformationJSON, err
	}

	// check status wallet real database
	IsDisabledUpdate, err := CheckConditionWallet("disabled", Token)

	if err != nil {
		return WalletInformationJSON, err
	}

	sql := `UPDATE ` + TableName + ` SET is_disabled = $1 WHERE token = $2`

	runQuery, err := tx.PrepareContext(ctx, sql)

	_, err = runQuery.ExecContext(ctx, IsDisabledUpdate, Token)

	if err == nil {

		// commit data
		tx.Commit()

		// Get Detail Information Wallet
		WalletInformationJSON, _ = GetDetailInfWalletNonActive(Token)

		return WalletInformationJSON, err

	} else {
		tx.Rollback()
		return WalletInformationJSON, err
	}
}

func GetDetailInfWalletActive(token string) (WalletInformationJSONData wallet.WalletStatusJSONActive, err error) {

	var WalletInformation wallet.WalletActiveResponse
	var WalletInformationJSON wallet.WalletStatusJSONActive

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		errors.New("[AuthenticationAPIKey]: Connection failure.")
	}

	sql := `
	SELECT id,customer_xid, is_disabled,  
	(SELECT action_at FROM history_wallets WHERE history_wallets.customer_xid = my_wallet.customer_xid ORDER BY history_wallets.action_at LIMIT 1) as enable_at,
	balance 
	FROM ` + TableName + ` WHERE token = $1`

	_ = db.QueryRow(sql, token).Scan(
		&WalletInformation.Id,
		&WalletInformation.Owned_by,
		&WalletInformation.StatusDB,
		&WalletInformation.EnableAt,
		&WalletInformation.Balance,
	)

	if (WalletInformation == wallet.WalletActiveResponse{}) {

		return WalletInformationJSON, err

	} else {

		// condition for status wallet
		WalletInformationJSON.Status = "enabled"

		// copy restult to json struct
		WalletInformationJSON.Id = WalletInformation.Id
		WalletInformationJSON.Owned_by = WalletInformation.Owned_by
		WalletInformationJSON.EnableAt = WalletInformation.EnableAt
		WalletInformationJSON.Balance = WalletInformation.Balance

		// push history data wallet
		err := ModelHistoryWallet.PushHistoryWallet(WalletInformation.Owned_by, false)

		return WalletInformationJSON, err
	}
}

func GetDetailInfWalletNonActive(token string) (WalletInformationJSONData wallet.WalletStatusJSONNonActive, err error) {

	var WalletInformation wallet.WalletNonActiveResponse
	var WalletInformationJSON wallet.WalletStatusJSONNonActive

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {
		errors.New("[AuthenticationAPIKey]: Connection failure.")
	}

	sql := `
	SELECT id,customer_xid, is_disabled,  
	(SELECT action_at FROM history_wallets WHERE history_wallets.customer_xid = my_wallet.customer_xid ORDER BY history_wallets.action_at LIMIT 1) as enable_at,
	balance 
	FROM ` + TableName + ` WHERE token = $1`

	_ = db.QueryRow(sql, token).Scan(
		&WalletInformation.Id,
		&WalletInformation.Owned_by,
		&WalletInformation.StatusDB,
		&WalletInformation.DisabledAt,
		&WalletInformation.Balance,
	)

	if (WalletInformation == wallet.WalletNonActiveResponse{}) {

		return WalletInformationJSON, err

	} else {

		WalletInformationJSON.Status = "disabled"

		// copy restult to json struct
		WalletInformationJSON.Id = WalletInformation.Id
		WalletInformationJSON.Owned_by = WalletInformation.Owned_by
		WalletInformationJSON.DisabledAt = WalletInformation.DisabledAt
		WalletInformationJSON.Balance = WalletInformation.Balance

		// push history data wallet
		err := ModelHistoryWallet.PushHistoryWallet(WalletInformation.Owned_by, true)

		return WalletInformationJSON, err
	}
}

/*
*	Check Validation status Wallet
 */
func CheckConditionWallet(CategoryAction string, token string) (IsDisabledUpdate bool, err error) {

	// call real status wallet in database
	RealStatusWallet, err := VerifiyStatusWallet(token)

	// for condition category action
	// is_disabled == true (disabled)
	if CategoryAction == "enabled" {

		if RealStatusWallet {

			IsDisabledUpdate = false

		} else {

			err = errors.New("Already " + CategoryAction + ".")

		}
	} else if CategoryAction == "disabled" {

		if !RealStatusWallet {

			IsDisabledUpdate = true

		} else {

			err = errors.New("Already " + CategoryAction + ".")

		}
	}

	return IsDisabledUpdate, err
}
