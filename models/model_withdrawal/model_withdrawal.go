package model_withdrawal

import (
	"context"
	"errors"
	"log"
	"mini_wallet/config"
	"mini_wallet/models/model_topup_wallet"
	"mini_wallet/models/types/withdrawal"
	"mini_wallet/util"
)

/*
* Create data wallet account and save to database
 */
func ModelWithdrawalProcess(RequestBody withdrawal.RequestBodyWithDrawal, APIKey string) (Response withdrawal.ResponseJSONWithDrawal, err error, result bool) {

	TableName := "history_deposits"
	StatusProcess := ""

	// set mark query 6 column
	MarkQuery := util.GenerateMarkQuery(7)
	GenUUID := util.GenerateUUID(true)
	TimeNow := util.TimeNow()

	db, err := config.ConnectDatabase()
	defer db.Close()

	// check amount
	if !util.AllowAmount(RequestBody.Amount) {

		err = errors.New("Amount is invalid.")
		return Response, err, false
	}

	CustomerXid, err := model_topup_wallet.GetCustomerXid(APIKey)

	if err != nil {
		return Response, err, false
	}

	// check balance real
	BalanceDB, _, err := CheckBalanceReal(CustomerXid, RequestBody.Amount)

	if err != nil {

		return Response, err, false
	}

	if BalanceDB == 0 {

		err = errors.New("Balance can't widthdrawal")

		return Response, err, false
	}

	// Cut Balance
	resultBalanceCut, err := BalanceToUpdate(CustomerXid, RequestBody.Amount)

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	sql := `INSERT INTO ` + TableName + ` (id, 
										   reference_id, 
										   customer_xid, 
										   status, 
										   action_at, 
										   category_action,
										   amount) 
										   VALUES (` + MarkQuery + `)`

	// run execute sql
	runQuery, err := tx.PrepareContext(ctx, sql)

	if err != nil {
		StatusProcess = "fail"
		log.Println("Err: " + err.Error())
	} else {
		StatusProcess = "success"
	}

	// run query saving
	_, err = runQuery.ExecContext(
		ctx,
		GenUUID,
		RequestBody.ReferenceId,
		CustomerXid,
		StatusProcess,
		TimeNow,
		"withdrawal",
		RequestBody.Amount,
	)

	// save to struct
	Response.Amount = RequestBody.Amount
	Response.Reference_id = RequestBody.ReferenceId
	Response.Withdrawn = CustomerXid
	Response.WithdrawnAt = TimeNow
	Response.StatusRequest = StatusProcess
	Response.Id = GenUUID

	if err != nil {
		tx.Rollback()

		err = errors.New("Failed push to history data deposits.")

		return Response, err, false
	} else {
		if resultBalanceCut {

			tx.Commit()

			return Response, err, true

		} else {

			tx.Rollback()
			err = errors.New("Failed to update balance.")

			return Response, err, false
		}
	}
}

/*
* Validate balance real for user / withdrawal
 */
func CheckBalanceReal(CustomerXid string, amount int) (balanceReal int, result bool, err error) {

	var AmountReal withdrawal.AmountReal

	TableName := "my_wallet"

	db, err := config.ConnectDatabase()
	defer db.Close()

	if err != nil {

		err = errors.New("[CheckBalanceReal]: Connection failure.")

		return 0, false, err

	}

	sql := `SELECT balance, is_disabled FROM ` + TableName + ` WHERE customer_xid = $1 AND is_disabled = false `

	_ = db.QueryRow(sql, CustomerXid).Scan(
		&AmountReal.Amount,
		&AmountReal.IsDisabled,
	)

	if AmountReal.Amount >= amount {

		return AmountReal.Amount, true, err

	} else {

		if AmountReal.IsDisabled {

			errors.New("Disabled.")
		}

		return 0, false, err
	}
}

/*
* Balance process cut
 */
func BalanceToUpdate(CustomerXid string, BalanceReal int) (result bool, err error) {

	db, err := config.ConnectDatabase()
	defer db.Close()

	TableName := "my_wallet"

	if err != nil {
		return false, err
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		return false, err
	}

	sql := `UPDATE ` + TableName + ` SET balance = balance - $1 WHERE customer_xid = $2 `

	runQuery, err := tx.PrepareContext(ctx, sql)

	_, err = runQuery.ExecContext(ctx, BalanceReal, CustomerXid)

	if err == nil {

		// commit data
		tx.Commit()

		return true, err

	} else {

		err = errors.New("[BalanceToUpdate]: failure balance update.")

		tx.Rollback()
		return false, err
	}
}
