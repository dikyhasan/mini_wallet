package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDatabaseConn(t *testing.T) {

	// try conn
	db, err := ConnectDatabase()
	defer db.Close()

	assert.Nil(t, err)

}
