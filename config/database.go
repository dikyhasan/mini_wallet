package config

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/lib/pq"
)

func ConnectDatabase() (*sql.DB, error) {

	DatabaseUrl := SetDatabaseUrl()

	db, err := sql.Open("postgres", DatabaseUrl)

	if err != nil {
		log.Println("Err: " + err.Error())
	}

	return db, err
}

func SetDatabaseUrl() string {

	EnvData := os.Getenv("CONNECTDB")

	var DbUrl = EnvData

	return DbUrl

}
